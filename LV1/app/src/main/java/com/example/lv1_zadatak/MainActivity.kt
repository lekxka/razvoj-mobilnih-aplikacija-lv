package com.example.lv1_zadatak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView1 = findViewById<TextView>(R.id.textView1)
        val textView2 = findViewById<TextView>(R.id.textView2)

        val inputText1 = findViewById<EditText>(R.id.editTextIme)
        val inputText2 = findViewById<EditText>(R.id.editTextOpis)

        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener{
            textView1.text = inputText1.text.toString()
            textView2.text = inputText2.text.toString()
        }

        val button2 = findViewById<Button>(R.id.button2)
        val visina = findViewById<EditText>(R.id.editTextHeight)
        val tezina = findViewById<EditText>(R.id.editTextWeight)


        button2.setOnClickListener{
            val v = visina.text.toString().toDouble()
            val t = tezina.text.toString().toDouble()

            val bmi : Double = t / (v*v)
            Toast.makeText(this, "%.3f".format(bmi), Toast.LENGTH_LONG).show()
        }
    }
}