package com.example.lv2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val secondActivityBtn = findViewById<Button>(R.id.button1)
        secondActivityBtn.setOnClickListener{
            val Intent = Intent(this, Activity2::class.java)
            startActivity(Intent)
        }
    }
}