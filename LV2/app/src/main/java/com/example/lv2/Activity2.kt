package com.example.lv2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Activity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val button1 = findViewById<Button>(R.id.button)
        button1.setOnClickListener{
            val firstFragment = Fragment1()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.fragmentContainerView, firstFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        val button2 = findViewById<Button>(R.id.button2)
        button2.setOnClickListener{
            val secondFragment = Fragment2()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.fragmentContainerView, secondFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

    }
}